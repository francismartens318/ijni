/**
 * 
 * Copyright (C) 2011, iDalko BVBA
 * All rights reserved.
 * 
 * THIS SOURCE CODE IS AN UNPUBLISHED PROPRIETARY TRADE SECRET OF iDalko BVBA, unless stated otherwise
 * in the license text.
 * 
 * The copyright notice above does not evidence any actual or intended publication of such source code.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE
 */
package com.idalko.jira.plugins.indexer;

import java.util.HashMap;

/**
* andrew Dec 10, 2010 5:16:12 PM
*/
class MapPropertiesHandler implements PropertiesHandler {
    private HashMap<String, String> properties;

    MapPropertiesHandler() {
        properties = new HashMap<String, String>();
    }

    public String getProperty(String name) {
        return properties.get(name);
    }

    public Integer getPropertyInt(String name) {
        Integer resultValue = 0;
        final String value = properties.get(name);
        if (value == null)
            return resultValue;

        try {
            resultValue = Integer.valueOf(value);
        } catch (NumberFormatException e) {
            // actually only integer value can be there, but in other case just use default value
        }
        return resultValue;
    }

    public void saveProperty(String name, String value) {
        properties.put(name, value);
    }

    public Boolean getBooleanProperty(String name) {
        final String s = properties.get(name);
        return PropertiesSetHandler.parseBoolean(s);
    }

    public HashMap<String, String> getProperties() {
        return properties;
    }

}
