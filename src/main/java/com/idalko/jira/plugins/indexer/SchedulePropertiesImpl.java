/**
 * 
 * Copyright (C) 2011, iDalko BVBA
 * All rights reserved.
 * 
 * THIS SOURCE CODE IS AN UNPUBLISHED PROPRIETARY TRADE SECRET OF iDalko BVBA, unless stated otherwise
 * in the license text.
 * 
 * The copyright notice above does not evidence any actual or intended publication of such source code.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE
 */
package com.idalko.jira.plugins.indexer;

/**
 * andrew Dec 2, 2010 11:08:54 AM
 */
public class SchedulePropertiesImpl implements ScheduleProperties {
    private PropertiesHandler propertiesHandler;

    public SchedulePropertiesImpl() {
        this(new PropertiesSetHandler());
    }

    public SchedulePropertiesImpl(PropertiesHandler propertiesHandler) {
        this.propertiesHandler = propertiesHandler;
    }

    public int getReindexingIntervalInMinutes() {
        return getPropertyInt(PropertiesHandler.REINDEXING_INTERVAL_IN_MINUTES);
    }

    public int getAnnouncedHour() {
        return getPropertyInt(PropertiesHandler.ANNOUNCED_HOUR);
    }

    public int getAnnounceMinutes() {
        return getPropertyInt(PropertiesHandler.ANNOUNCE_MINUTES);
    }

    public int getIndexingHour() {
        return getPropertyInt(PropertiesHandler.INDEXING_HOUR);
    }

    public int getIndexingMinute() {
        return getPropertyInt(PropertiesHandler.INDEXING_MINUTE);
    }

    public int getStopReindexerHour() {
        return getPropertyInt(PropertiesHandler.STOP_REINDEXER_HOUR);
    }

    public int getStopReindexerMinute() {
        return getPropertyInt(PropertiesHandler.STOP_REINDEXER_MINUTE);
    }

    public String getAnnouncementText() {
        final String announcementText = PropertiesHandler.ANNOUNCEMENT_TEXT;
        return getProperty(announcementText);
    }


    public Boolean getIsCheckUsersOnline() {
        return getBooleanProperty(PropertiesHandler.IS_CHECK_USERS_ONLINE);
    }

    public Boolean getIsIndexingNecessary() {
        return getBooleanProperty(PropertiesHandler.IS_INDEXING_NECESSARY);
    }

    public Boolean getIsEnable() {
        return getBooleanProperty(PropertiesHandler.IS_ENABLE);
    }

    public void setIsEnable(Boolean isEnable) {
        propertiesHandler.saveProperty(PropertiesHandler.IS_ENABLE, String.valueOf(isEnable));
    }

    public void setDefaultBanner(String announcement) {
        saveProperty(PropertiesHandler.CURRENT_ANNOUNCEMENT, announcement);
    }

    public void setDefaultVisibility(String bannerVisibility) {
        saveProperty(PropertiesHandler.BANNER_VISIBILITY, bannerVisibility);
    }

    public boolean isOwnBannerShown() {
        return getBooleanProperty(PropertiesHandler.OWN_BANNER_SHOWN);
    }

    public void setOwnBannerShown(Boolean value) {
        saveProperty(PropertiesHandler.OWN_BANNER_SHOWN, String.valueOf(value));
    }

    public String getDefaultBanner() {
        return getProperty(PropertiesHandler.CURRENT_ANNOUNCEMENT);
    }

    public String getDefaultVisibility() {
        return getProperty(PropertiesHandler.BANNER_VISIBILITY);
    }

    public String getProperty(String name) {
        return propertiesHandler.getProperty(name);
    }

    public Integer getPropertyInt(String name) {
        return propertiesHandler.getPropertyInt(name);
    }

    public void saveProperty(String name, String value) {
        propertiesHandler.saveProperty(name, value);
    }

    public Boolean getBooleanProperty(String name) {
        return propertiesHandler.getBooleanProperty(name);
    }

    public void copyTo(ScheduleProperties target) {
        final String[] properties = PropertiesHandler.list;
        for (String name : properties) {
            final String newValue = getProperty(name);
            target.saveProperty(name, newValue);
        }
    }
}
