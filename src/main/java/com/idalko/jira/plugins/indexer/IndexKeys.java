/**
 * 
 * Copyright (C) 2011, iDalko BVBA
 * All rights reserved.
 * 
 * THIS SOURCE CODE IS AN UNPUBLISHED PROPRIETARY TRADE SECRET OF iDalko BVBA, unless stated otherwise
 * in the license text.
 * 
 * The copyright notice above does not evidence any actual or intended publication of such source code.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE
 */
package com.idalko.jira.plugins.indexer;

/**
 * andrew Nov 26, 2010 6:18:43 PM
 */
public interface IndexKeys {
    String ANNOUNCEMENT_TEXT = "idalko.announcementBanner.text";
    String ANNOUNCEMENT_VISIBILITY = "idalko.announcementBanner.visibility";

    // make string localization
    String STOP_TRIGGER_NAME = "idalko.scheduler.stop.triggerName";
    String BANNER_TRIGGER_NAME = "idalko.scheduler.banner.triggerName";
    String TRIGGER_NAME = "idalko.scheduler.triggerName";
    String JOB_NAME = "idalko.scheduler.jobName";
    String GROUP_NAME = "idalko.scheduler.groupName";
    String BANNER_JOB_NAME = "idalko.scheduler.banner.jobName";
    String REINDEX_CHECKER_TRIGGER = "idalko.scheduler.reindex.checker.trigger";
    String STOP_JOB_NAME = "idalko.scheduler.stop.jobName";
}
