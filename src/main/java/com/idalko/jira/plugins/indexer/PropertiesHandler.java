/**
 * 
 * Copyright (C) 2011, iDalko BVBA
 * All rights reserved.
 * 
 * THIS SOURCE CODE IS AN UNPUBLISHED PROPRIETARY TRADE SECRET OF iDalko BVBA, unless stated otherwise
 * in the license text.
 * 
 * The copyright notice above does not evidence any actual or intended publication of such source code.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE
 */
package com.idalko.jira.plugins.indexer;

/**
* andrew Dec 10, 2010 3:55:39 PM
*/
public interface PropertiesHandler {

    String STOP_REINDEXER_MINUTE = "stopReindexerMinute";
    String STOP_REINDEXER_HOUR = "stopReindexerHour";
    String INDEXING_MINUTE = "indexingMinute";
    String INDEXING_HOUR = "indexingHour";
    String ANNOUNCE_MINUTES = "announceMinutes";
    String ANNOUNCED_HOUR = "announcedHour";
    String REINDEXING_INTERVAL_IN_MINUTES = "reindexingIntervalInMinutes";
    String ANNOUNCEMENT_TEXT = "announcementText";
    String CURRENT_ANNOUNCEMENT = "activeAnnouncement";
    String OWN_BANNER_SHOWN = "ownBannerIsShown";
    String BANNER_VISIBILITY = "activeBannerVisibility";
    String IS_CHECK_USERS_ONLINE = "isCheckUsersOnline";
    String IS_INDEXING_NECESSARY= "isIndexingNecessary";
    String IS_ENABLE = "isEnable";
    String[] list = new String[]{
            STOP_REINDEXER_MINUTE,
            STOP_REINDEXER_HOUR,
            INDEXING_MINUTE,
            INDEXING_HOUR, ANNOUNCE_MINUTES,
            ANNOUNCED_HOUR, REINDEXING_INTERVAL_IN_MINUTES,
            ANNOUNCEMENT_TEXT,
            IS_CHECK_USERS_ONLINE,
            IS_INDEXING_NECESSARY};

    String getProperty(String announcementText);

    Integer getPropertyInt(String param);

    void saveProperty(String name, String value);

    Boolean getBooleanProperty(String checkUsersOnline);

}
