/**
 *
 * Copyright (C) 2011, iDalko BVBA
 * All rights reserved.
 *
 * THIS SOURCE CODE IS AN UNPUBLISHED PROPRIETARY TRADE SECRET OF iDalko BVBA, unless stated otherwise
 * in the license text.
 *
 * The copyright notice above does not evidence any actual or intended publication of such source code.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE
 */
package com.idalko.jira.plugins.indexer;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.action.ProjectActionSupport;
import org.apache.commons.lang.StringUtils;
import org.quartz.Trigger;
import org.quartz.helpers.TriggerUtils;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

/**
 * andrew Dec 1, 2010 1:07:51 PM
 */
public class ScheduledIndexingAction extends ProjectActionSupport {
    private ScheduleProperties currentProperties;
    private ScheduleProperties scheduleSavedProperties;
    private SchedulerService schedulerService;
    private IndexAdminManager adminManager;
    private IndexService indexService;

    public ScheduledIndexingAction(SchedulerService schedulerService, IndexAdminManager adminManager, IndexService indexService) {
        this.schedulerService = schedulerService;
        this.adminManager = adminManager;
        this.indexService = indexService;
        currentProperties = new SchedulePropertiesImpl(new MapPropertiesHandler());
        scheduleSavedProperties = schedulerService.getScheduleProperties();
    }


    public String doCancel() {
        if (!getCanEditConfig()) {
            reportError("error.announce", "idalko.administration.action.denied");
            return SUCCESS;
        }

        clear(getErrors());
        clear(getErrorMessages());

        scheduleSavedProperties.copyTo(currentProperties);
        return SUCCESS;
    }

    private void clear(Map errors) {
        if (errors != null) {
            errors.clear();
        }
    }

    private void clear(Collection errors) {
        if (errors != null) {
            errors.clear();
        }
    }

    public String doUpdate() {
        if (!getCanEditConfig()) {
            reportError("error.announce", "idalko.administration.action.denied");
            return SUCCESS;
        }

        if (request.getParameter("cancel") != null)
            return doCancel();
        final String[] properties = PropertiesHandler.list;
        for (String property : properties) {
            final String newValue = request.getParameter(property);
            currentProperties.saveProperty(property, newValue);
        }

        final Date announcementNextFireTime = getNextAnnouncementFireTime();
        final Date indexingNextFireTime = getNextIndexingFireTime(currentProperties);

        if (announcementNextFireTime != null && announcementNextFireTime.after(indexingNextFireTime)) {
            reportError("error.announce", "idalko.administration.error.announce");
        }

        final Date stopNextFireTime = getNextFireTime(currentProperties.getStopReindexerHour(), currentProperties.getStopReindexerMinute());
        if (stopNextFireTime != null && stopNextFireTime.before(indexingNextFireTime)) {
            reportError("error.stop", "idalko.administration.error.stop");
        }

        final String announcementText = currentProperties.getAnnouncementText();
        if (!StringUtils.isEmpty(announcementText) && announcementText.length() > 255) {
            reportError("error.announce.length", "idalko.administration.error.announce.length");
        }

        if (hasAnyErrors()) {
            return ERROR;
        }

        currentProperties.copyTo(scheduleSavedProperties);

        removeOwnBanner();
        schedulerService.rescheduleAll();
        return SUCCESS;
    }

    private void reportError(String fieldName, String messageKey) {
        final String message = getText(messageKey);
        addErrorMessage(message);
        addError(fieldName, message);
    }

    private void reportError(String messageKey) {
        final String message = getText(messageKey);
        addErrorMessage(message);
    }

    private void removeOwnBanner() {
        if (scheduleSavedProperties.isOwnBannerShown()) {
            adminManager.removeBanner(scheduleSavedProperties);
            scheduleSavedProperties.setOwnBannerShown(false);
        }
    }

    private Date getNextIndexingFireTime(ScheduleProperties properties) {
        return getNextFireTime(properties.getIndexingHour(), properties.getIndexingMinute());
    }

    private Date getNextAnnouncementFireTime() {
        return getNextFireTime(currentProperties.getAnnouncedHour(), currentProperties.getAnnounceMinutes());
    }

    private Date getNextFireTime(int hour, int minute) {
        final Trigger announcementTrigger = TriggerUtils.makeDailyTrigger(hour, minute);
        announcementTrigger.setStartTime(new Date());
        return announcementTrigger.getFireTimeAfter(new Date());
    }

    @Override
    public String getText(String key) {
        I18nHelper i18nHelper = ComponentManager.getInstance().getJiraAuthenticationContext().getI18nHelper();
        return i18nHelper.getText(key);
    }

    public ScheduleProperties getProperties() {
        return currentProperties;
    }

    public ScheduleProperties getSavedProperties() {
        return scheduleSavedProperties;
    }


    public String doEnable() throws Exception {
        if (!getCanEditConfig()) {
            reportError("error.announce", "idalko.administration.action.denied");
            return SUCCESS;
        }

        scheduleSavedProperties.setIsEnable(Boolean.TRUE);
        schedulerService.rescheduleAll();
        final Date now = new Date();

        final Date indexingFireTime = getNextIndexingFireTime(scheduleSavedProperties);
        final Calendar nextAnnounce = Calendar.getInstance();
        nextAnnounce.set(Calendar.HOUR_OF_DAY, scheduleSavedProperties.getAnnouncedHour());
        nextAnnounce.set(Calendar.MINUTE, scheduleSavedProperties.getAnnounceMinutes());
        nextAnnounce.set(Calendar.SECOND, 0);
        final Date announcementTriggerFireTimeAfter = nextAnnounce.getTime();


        // check if same run is in focus
        long difference = Math.abs(indexingFireTime.getTime() - now.getTime()) / (60 * 60 * 1000);

        if (now.after(announcementTriggerFireTimeAfter) && now.before(indexingFireTime) && difference < 12) {
            indexService.announce();
        }
        return SUCCESS;
    }

    public String doDisable() throws Exception {
        scheduleSavedProperties.setIsEnable(Boolean.FALSE);
        schedulerService.unscheduleAll();
        removeOwnBanner();
        return SUCCESS;
    }

    public String doEdit() {
        scheduleSavedProperties.copyTo(currentProperties);
        return SUCCESS;
    }

    public boolean getCanEditConfig() {
        return isSystemAdministrator();
    }
}