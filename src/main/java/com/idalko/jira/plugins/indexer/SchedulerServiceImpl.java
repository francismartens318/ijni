/**
 * 
 * Copyright (C) 2011, iDalko BVBA
 * All rights reserved.
 * 
 * THIS SOURCE CODE IS AN UNPUBLISHED PROPRIETARY TRADE SECRET OF iDalko BVBA, unless stated otherwise
 * in the license text.
 * 
 * The copyright notice above does not evidence any actual or intended publication of such source code.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE
 */
package com.idalko.jira.plugins.indexer;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.ManagerFactory;
import com.atlassian.jira.util.I18nHelper;
import com.idalko.jira.plugins.indexer.jobs.AnnounceIndexingJob;
import com.idalko.jira.plugins.indexer.jobs.IndexingJob;
import com.idalko.jira.plugins.indexer.jobs.StopReIndexerJob;
import org.apache.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.helpers.TriggerUtils;

import java.util.Date;

/**
 * andrew Nov 26, 2010 3:40:06 PM
 */
public class SchedulerServiceImpl implements SchedulerService {
    private static final Logger log = Logger.getLogger(SchedulerServiceImpl.class);
    private Scheduler scheduler;

    public final String STOP_TRIGGER;
    public final String BANNER_TRIGGER;
    public final String INDEXER_TRIGGER;
    public final String INDEXER_JOB;
    public final String GROUP_NAME;
    public final String BANNER_JOB;
    public final String REINDEXER_TRIGGER;
    public final String STOP_JOB;
    private ScheduleProperties scheduleProperties;

    public SchedulerServiceImpl() {
        scheduleProperties = createProperties();

        scheduler = getScheduler();

        I18nHelper i18n = getI18Helper();
        STOP_TRIGGER = i18n.getText(IndexKeys.STOP_TRIGGER_NAME);
        BANNER_TRIGGER = i18n.getText(IndexKeys.BANNER_TRIGGER_NAME);
        INDEXER_TRIGGER = i18n.getText(IndexKeys.TRIGGER_NAME);
        INDEXER_JOB = i18n.getText(IndexKeys.JOB_NAME);
        GROUP_NAME = i18n.getText(IndexKeys.GROUP_NAME);
        BANNER_JOB = i18n.getText(IndexKeys.BANNER_JOB_NAME);
        REINDEXER_TRIGGER = i18n.getText(IndexKeys.REINDEX_CHECKER_TRIGGER);
        STOP_JOB = i18n.getText(IndexKeys.STOP_JOB_NAME);

        if (scheduleProperties.getIsEnable())
            rescheduleAll();
    }

    protected ScheduleProperties createProperties() {
        return new SchedulePropertiesImpl();
    }

    public void rescheduleAll() {
        log.debug("Make scheduling of jobs");
        // schedule show announcement job
        scheduleShowBannerJob();
        // indexer itself
        scheduleIndexerJob();
        // indexer  stopper
        scheduleStopJob();

        try {
            unschedule();
        } catch (Exception e) {
            log.warn("Error occured while stopping reindexer", e);
        }
    }


    protected I18nHelper getI18Helper() {
        return ComponentManager.getInstance().getJiraAuthenticationContext().getI18nHelper();
    }

    protected Scheduler getScheduler() {
        return ManagerFactory.getScheduler();
    }


    public void unschedule() throws SchedulerException {
        log.debug("ReIndexer checker stopped");
        scheduler.unscheduleJob(REINDEXER_TRIGGER, GROUP_NAME);
    }


    public void reschedule() {
        log.debug("Start indexing rescheduling");

        // at first, check if we over jump stop indexer trigger
        try {
            final Trigger trigger = scheduler.getTrigger(REINDEXER_TRIGGER, GROUP_NAME);
            if (trigger == null) {
                final Trigger reIndexChecker = createTrigger(INDEXER_JOB, REINDEXER_TRIGGER,
                        TriggerUtils.makeMinutelyTrigger(scheduleProperties.getReindexingIntervalInMinutes()));
                reIndexChecker.setEndTime(TriggerUtils.getDateOf(0, scheduleProperties.getStopReindexerMinute(), scheduleProperties.getStopReindexerHour()));
                scheduler.scheduleJob(reIndexChecker);
            }
        } catch (SchedulerException e) {
            log.warn("Error while scheduling reindex process", e);
        }
        log.debug("Indexing postponed");
    }


    protected void scheduleDaily(String jobName, Class jobClass, String triggerName, int hour, int minute) {
        JobDetail jobDetail = new JobDetail(jobName, GROUP_NAME, jobClass, false, true, false);

        try {
            final Trigger trigger = createTrigger(jobName, triggerName, TriggerUtils.makeDailyTrigger(hour, minute));

            unschedule(jobName, triggerName);
            scheduler.scheduleJob(jobDetail, trigger);


        } catch (Exception e) {
            log.error("Error while scheduling show job", e);
        }
    }

    private void unschedule(String jobName, String triggerName) throws SchedulerException {
        final JobDetail detail = scheduler.getJobDetail(jobName, GROUP_NAME);

        if (detail != null) {
            scheduler.deleteJob(jobName, GROUP_NAME);
            scheduler.unscheduleJob(triggerName, GROUP_NAME);
        }
    }

    private Trigger createTrigger(String scheduledJobName, String scheduledTriggerName, Trigger trigger) {
        trigger.setName(scheduledTriggerName);
        trigger.setJobName(scheduledJobName);
        trigger.setGroup(GROUP_NAME);
        trigger.setJobGroup(GROUP_NAME);
        trigger.setStartTime(TriggerUtils.getEvenMinuteDate(getDate()));
        return trigger;
    }

    protected Date getDate() {
        return new Date();
    }

    protected void scheduleStopJob() {
        scheduleDaily(STOP_JOB, StopReIndexerJob.class, STOP_TRIGGER, scheduleProperties.getStopReindexerHour(), scheduleProperties.getStopReindexerMinute());
    }

    protected void scheduleIndexerJob() {
        scheduleDaily(INDEXER_JOB, IndexingJob.class, INDEXER_TRIGGER, scheduleProperties.getIndexingHour(), scheduleProperties.getIndexingMinute());
    }

    protected void scheduleShowBannerJob() {
        scheduleDaily(BANNER_JOB, AnnounceIndexingJob.class, BANNER_TRIGGER, scheduleProperties.getAnnouncedHour(), scheduleProperties.getAnnounceMinutes());
    }


    public ScheduleProperties getScheduleProperties() {
        return scheduleProperties;
    }

    public void unscheduleAll() throws SchedulerException {
        unschedule(STOP_JOB, STOP_TRIGGER);
        unschedule(INDEXER_JOB, INDEXER_TRIGGER);
        unschedule(INDEXER_JOB, REINDEXER_TRIGGER);
        unschedule(BANNER_JOB, BANNER_TRIGGER);
    }
}
