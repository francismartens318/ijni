/**
 * 
 * Copyright (C) 2011, iDalko BVBA
 * All rights reserved.
 * 
 * THIS SOURCE CODE IS AN UNPUBLISHED PROPRIETARY TRADE SECRET OF iDalko BVBA, unless stated otherwise
 * in the license text.
 * 
 * The copyright notice above does not evidence any actual or intended publication of such source code.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE
 */
package com.idalko.jira.plugins.indexer;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.ReindexMessageManager;
import com.opensymphony.user.User;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntityException;
import org.quartz.SchedulerException;


import java.text.MessageFormat;

/**
 * andrew Nov 22, 2010 6:37:24 PM
 */
public class IndexServiceImpl implements IndexService {

    private IndexAdminManager indexAdminManager;
    private SchedulerService schedulerService;


    private static final Logger log = Logger.getLogger(IndexServiceImpl.class);

    public IndexServiceImpl(IndexAdminManager indexAdminManager, SchedulerService schedulerService) throws SchedulerException, GenericEntityException {
        this.indexAdminManager = indexAdminManager;
        this.schedulerService = schedulerService;

    }

    public boolean reindex() {
        log.debug("Indexing started....");
        final ScheduleProperties scheduleProperties = schedulerService.getScheduleProperties();
        boolean isAllUsersInactive = indexAdminManager.isUsersInactive(scheduleProperties);
        if (!isAllUsersInactive) {
            log.debug(MessageFormat.format("Users sessions activity found in last {0} minutes.", USER_TIME_INACTIVE));
            schedulerService.reschedule();
            return false;
        }

        log.debug("No active user sessions found. Start indexing");

        try {
            try {
                schedulerService.unschedule();
            } catch (SchedulerException e) {
                log.warn("Error while scheduling indexing process", e);
            }
            if (isIndexingNecessary() && scheduleProperties.getIsIndexingNecessary()
                    ||  !scheduleProperties.getIsIndexingNecessary()) {
                log.debug("Indexing started");
                indexAdminManager.startIndexing();
            }
            indexAdminManager.removeBanner(scheduleProperties);
        } catch (Exception e) {
            log.warn("Error occurs while running index command  ", e);
        }

        return true;
    }

    public void setIndexAdminManager(IndexAdminManager indexAdminManager) {
        this.indexAdminManager = indexAdminManager;
    }

    public void announce() {
        log.debug("Announce future indexing for users...");
        indexAdminManager.showBanner(schedulerService.getScheduleProperties());
    }

    public void stop() {
        log.debug("Stop checking possibilities for indexing");
        try {
            schedulerService.unschedule();
            indexAdminManager.removeBanner(schedulerService.getScheduleProperties());
        } catch (SchedulerException e) {
            log.warn("Error while scheduling reindex process", e);
        }
    }


    public boolean isIndexingNecessary(){
        ReindexMessageManager reindexMessageManager = ComponentManager.getComponent(ReindexMessageManager.class);
        if (reindexMessageManager == null)
            return true;

        final String message = reindexMessageManager.getMessage((User) null);
        return message != null;
    }
}
