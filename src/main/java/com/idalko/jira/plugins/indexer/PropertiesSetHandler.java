/**
 *
 * Copyright (C) 2011, iDalko BVBA
 * All rights reserved.
 *
 * THIS SOURCE CODE IS AN UNPUBLISHED PROPRIETARY TRADE SECRET OF iDalko BVBA, unless stated otherwise
 * in the license text.
 *
 * The copyright notice above does not evidence any actual or intended publication of such source code.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE
 */
package com.idalko.jira.plugins.indexer;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.util.I18nHelper;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * andrew Dec 10, 2010 5:14:50 PM
 */
class PropertiesSetHandler implements PropertiesHandler {
    public static final String ENTITY_NAME = "iDalko";
    public static final long ENTITY_ID = 1L;
    private PropertySet propertySet;


    public String getProperty(String name) {
        return getPropertySet().getText(name);
    }

    public Integer getPropertyInt(String name) {
        return getPropertyInteger(name);
    }

    public void saveProperty(String name, String value) {
        getPropertySet().setText(name, value);
    }

    public Boolean getBooleanProperty(String name) {
        final String text = getPropertySet().getText(name);
        return parseBoolean(text);
    }

    private static PropertySet getPropertySet(String entityName, Long entityId) {
        PropertySet ofbizPs = PropertySetManager.getInstance("ofbiz", buildPropertySet(entityName, entityId));
        HashMap<String, Object> args = new HashMap<String, Object>();
        args.put("PropertySet", ofbizPs);
        args.put("bulkload", true);
        return PropertySetManager.getInstance("cached", args);
    }

    private static Map<String, Serializable> buildPropertySet(String entityName, Long entityId) {
        HashMap<String, Serializable> ofbizArgs = new HashMap<String, Serializable>();
        ofbizArgs.put("delegator.name", "default");
        ofbizArgs.put("entityName", entityName);
        ofbizArgs.put("entityId", entityId);
        return ofbizArgs;
    }

    public synchronized PropertySet getPropertySet() {
        if (propertySet == null) {
            propertySet = getPropertySet(ENTITY_NAME, ENTITY_ID);
            updateNullEntities();
        }
        return propertySet;
    }

    private void updateNullEntities() {
        setDefaultValue(STOP_REINDEXER_MINUTE, ScheduleProperties.STOP_REINDEXER_MINUTE);
        setDefaultValue(STOP_REINDEXER_HOUR, ScheduleProperties.STOP_REINDEXER_HOUR);
        setDefaultValue(INDEXING_MINUTE, ScheduleProperties.INDEXING_MINUTE);
        setDefaultValue(INDEXING_HOUR, ScheduleProperties.INDEXING_HOUR);
        setDefaultValue(ANNOUNCE_MINUTES, ScheduleProperties.ANNOUNCE_MINUTES);
        setDefaultValue(ANNOUNCED_HOUR, ScheduleProperties.ANNOUNCED_HOUR);
        setDefaultValue(REINDEXING_INTERVAL_IN_MINUTES, ScheduleProperties.REINDEXING_INTERVAL_IN_MINUTES);


        final I18nHelper helper = ComponentManager.getInstance().getJiraAuthenticationContext().getI18nHelper();
        if (!getPropertySet().exists(ANNOUNCEMENT_TEXT) || getPropertySet().getText(IndexKeys.ANNOUNCEMENT_TEXT) == null) {
            saveProperty(ANNOUNCEMENT_TEXT, helper.getText(IndexKeys.ANNOUNCEMENT_TEXT));
        }

        saveBooleanProperty(IS_CHECK_USERS_ONLINE);
        saveBooleanProperty(IS_INDEXING_NECESSARY);
    }

    private void saveBooleanProperty(String propertyName) {
        if (!getPropertySet().exists(propertyName) || getPropertySet().getText(propertyName) == null) {
            saveProperty(propertyName, Boolean.TRUE.toString());
        }
    }

    private void setDefaultValue(String paramName, int value) {
        if (!getPropertySet().exists(paramName) || getPropertySet().getText(paramName) == null) {
            savePropertyInteger(paramName, value);
        }
    }

    public void savePropertyInteger(String name, Integer value) {
        final String valueString = String.valueOf(value);
        saveProperty(name, valueString);
    }

    public Integer getPropertyInteger(String param) {
        return getPropertyInteger(param, 0);
    }

    public Integer getPropertyInteger(String param, Integer defaultValue) {
        Integer intValue = defaultValue;
        try {
            intValue = getIntProperty(param);
            if (intValue == null)
                return defaultValue;
        } catch (NumberFormatException e) {
            propertySet.remove(param);
        }
        return intValue;
    }

    private Integer getIntProperty(String paramName) {
        if (propertySet.exists(paramName)) {
            return Integer.valueOf(propertySet.getText(paramName));
        } else
            return null;
    }

    static boolean parseBoolean(String valueString) {
        return valueString != null && (Boolean.TRUE.toString().equals(valueString) || valueString.equals("on"));
    }
}
