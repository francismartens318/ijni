/**
 * 
 * Copyright (C) 2011, iDalko BVBA
 * All rights reserved.
 * 
 * THIS SOURCE CODE IS AN UNPUBLISHED PROPRIETARY TRADE SECRET OF iDalko BVBA, unless stated otherwise
 * in the license text.
 * 
 * The copyright notice above does not evidence any actual or intended publication of such source code.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE
 */
package com.idalko.jira.plugins.indexer;

/**
 * andrew Dec 1, 2010 11:01:03 AM
 */
public interface ScheduleProperties {
    // restart indexing each specified minutes
    int REINDEXING_INTERVAL_IN_MINUTES = 1;

    // show announcement banner at
    int ANNOUNCED_HOUR = 21;
    int ANNOUNCE_MINUTES = 30;

    // start indexing at
    int INDEXING_HOUR = 22;
    int INDEXING_MINUTE = 0;
    // stop indexing attempts at

    int STOP_REINDEXER_HOUR = 23;
    int STOP_REINDEXER_MINUTE = 59;

    int getReindexingIntervalInMinutes();

    int getAnnouncedHour();

    int getAnnounceMinutes();

    int getIndexingHour();

    int getIndexingMinute();

    int getStopReindexerHour();

    int getStopReindexerMinute();

    String getAnnouncementText();

    Boolean getIsCheckUsersOnline();

    void setDefaultBanner(String announcement);

    void setDefaultVisibility(String bannerVisibility);

    boolean isOwnBannerShown();

    String getDefaultBanner();

    String getDefaultVisibility();

    void setOwnBannerShown(Boolean value);

    void saveProperty(String name, String value);

    Integer getPropertyInt(String param);

    String getProperty(String announcementText);

    Boolean getBooleanProperty(String checkUsersOnline);

    void copyTo(ScheduleProperties target);

    Boolean getIsEnable();

    void setIsEnable(Boolean isEnable);

    Boolean getIsIndexingNecessary();

}
