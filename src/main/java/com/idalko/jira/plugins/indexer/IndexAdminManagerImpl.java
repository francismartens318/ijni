/**
 *
 * Copyright (C) 2011, iDalko BVBA
 * All rights reserved.
 *
 * THIS SOURCE CODE IS AN UNPUBLISHED PROPRIETARY TRADE SECRET OF iDalko BVBA, unless stated otherwise
 * in the license text.
 *
 * The copyright notice above does not evidence any actual or intended publication of such source code.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE
 */
package com.idalko.jira.plugins.indexer;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.IndexPathManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.util.FileFactory;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.index.IndexLifecycleManager;
import com.atlassian.jira.web.ServletContextProvider;
import com.atlassian.jira.web.action.admin.index.IndexAdminImpl;
import com.atlassian.jira.web.session.currentusers.JiraUserSession;
import com.atlassian.jira.web.session.currentusers.JiraUserSessionTracker;
import com.atlassian.jira.web.util.OutlookDateManager;
import org.apache.log4j.Logger;
import webwork.action.ServletActionContext;

import javax.servlet.ServletContext;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * andrew Nov 26, 2010 6:04:01 PM
 */
public class IndexAdminManagerImpl implements IndexAdminManager {
    private IndexLifecycleManager indexLifecycleManager;
    private GlobalPermissionManager globalPermissionManager;
    private TaskManager taskManager;
    private JiraAuthenticationContext authenticationContext;
    private OutlookDateManager outlookDateManager;
    private IndexPathManager indexPathManager;
    private static final Logger log = Logger.getLogger(IndexAdminManagerImpl.class);

    private ApplicationProperties applicationProperties;
    private I18nHelper i18n;


    public IndexAdminManagerImpl(IndexLifecycleManager indexLifecycleManager,
                                 GlobalPermissionManager globalPermissionManager,
                                 TaskManager taskManager,
                                 JiraAuthenticationContext authenticationContext,
                                 OutlookDateManager outlookDateManager,
                                 IndexPathManager indexPathManager,
                                 final ApplicationProperties applicationProperties) {
        this.indexLifecycleManager = indexLifecycleManager;
        this.globalPermissionManager = globalPermissionManager;
        this.taskManager = taskManager;
        this.authenticationContext = authenticationContext;
        this.i18n = authenticationContext.getI18nHelper();
        this.outlookDateManager = outlookDateManager;
        this.indexPathManager = indexPathManager;
        this.applicationProperties = applicationProperties;

        log.debug("Service instantiation completed");
    }

    public String startIndexing() throws Exception {
        final ServletContext servletContext = ServletContextProvider.getServletContext();

        if (servletContext != null) {
            ServletActionContext.setServletContext(servletContext);
        }

        FileFactory fileFactory = ComponentManager.getComponent(FileFactory.class);
        final IndexAdminImpl indexAdmin = new IndexAdminImpl(indexLifecycleManager,
                globalPermissionManager,
                taskManager,
                authenticationContext,
                outlookDateManager,
                indexPathManager, fileFactory) {
            @Override
            public boolean isHasSystemAdminPermission() {
                return true;
            }

            @Override
            public String getRedirect(String defaultUrl) {
                return defaultUrl;
            }
        };

        return indexAdmin.doReindex();
    }

    public boolean isUsersInactive(ScheduleProperties properties) {
        if (!properties.getIsCheckUsersOnline())
            return true;

        final JiraUserSessionTracker jiraUserSessionTracker = JiraUserSessionTracker.getInstance();
        final List<JiraUserSession> userSessions = jiraUserSessionTracker.getSnapshot();
        boolean isAllUsersInactive = true;
        log.debug("Checking for active user sessions....");
        if (userSessions != null && !userSessions.isEmpty()) {


            final Calendar nowCalendar = Calendar.getInstance();
            final Calendar userCalendar = Calendar.getInstance();
            for (JiraUserSession userSession : userSessions) {
                final Date lastAccessTime = userSession.getLastAccessTime();
                userCalendar.setTime(lastAccessTime);
                userCalendar.add(Calendar.MINUTE, properties.getReindexingIntervalInMinutes());

                if (userCalendar.after(nowCalendar)) {
                    isAllUsersInactive = false;
                    break;
                }
            }
        }

        return isAllUsersInactive;
    }

    public void removeBanner(ScheduleProperties properties) {
        String announcement = properties.getDefaultBanner();
        String bannerVisibility = properties.getDefaultVisibility();

        setBanner(announcement, bannerVisibility);
        properties.setOwnBannerShown(false);
        log.debug("Announcement banner removed");
    }

    private void setBanner(String announcementString, String visibilityString) {
        applicationProperties.setText(APKeys.JIRA_ALERT_HEADER, announcementString);
        applicationProperties.setText(APKeys.JIRA_ALERT_HEADER_VISIBILITY, visibilityString);
    }

    public void showBanner(ScheduleProperties properties) {
        String bannerVisibility = applicationProperties.getDefaultBackedString(APKeys.JIRA_ALERT_HEADER_VISIBILITY);
        String announcement = applicationProperties.getDefaultBackedString(APKeys.JIRA_ALERT_HEADER);
        final String text = properties.getAnnouncementText();

        if (!properties.isOwnBannerShown()) {
            properties.setDefaultBanner(announcement);
            properties.setDefaultVisibility(bannerVisibility);
            properties.setOwnBannerShown(true);
        }

        final String visibilityString = i18n.getText(IndexKeys.ANNOUNCEMENT_VISIBILITY);
        setBanner(text, visibilityString);
    }
}
