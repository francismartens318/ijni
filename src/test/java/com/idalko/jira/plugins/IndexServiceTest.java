/**
 *
 * Copyright (C) 2011, iDalko BVBA
 * All rights reserved.
 *
 * THIS SOURCE CODE IS AN UNPUBLISHED PROPRIETARY TRADE SECRET OF iDalko BVBA, unless stated otherwise
 * in the license text.
 *
 * The copyright notice above does not evidence any actual or intended publication of such source code.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE
 */
package com.idalko.jira.plugins;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.config.util.IndexPathManager;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.task.TaskManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.index.IndexLifecycleManager;
import com.atlassian.jira.web.util.OutlookDateManager;
import com.idalko.jira.plugins.indexer.*;
import com.idalko.jira.plugins.indexer.jobs.AnnounceIndexingJob;
import com.idalko.jira.plugins.indexer.jobs.IndexingJob;
import com.idalko.jira.plugins.indexer.jobs.StopReIndexerJob;
import junit.framework.TestCase;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.jmock.integration.junit4.JUnit4Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.core.QuartzScheduler;
import org.quartz.core.QuartzSchedulerResources;
import org.quartz.core.SchedulingContext;
import org.quartz.impl.StdScheduler;
import org.quartz.simpl.RAMJobStore;

import java.util.Calendar;
import java.util.Date;

@RunWith(JMock.class)
public class IndexServiceTest extends TestCase {
    Mockery context = new JUnit4Mockery() {
        {
            this.setImposteriser(ClassImposteriser.INSTANCE);
        }
    };
    private IndexServiceImpl indexService;
    private SchedulerServiceImpl schedulerService;
    private StdScheduler scheduler;
    private I18nHelper i18n;
    private IndexLifecycleManager indexLifecycleManager;
    private GlobalPermissionManager globalPermissionManager;
    private TaskManager taskManager;
    private JiraAuthenticationContext authenticationContext;
    private OutlookDateManager outlookDateManager;
    private IndexPathManager indexPathManager;
    private ApplicationProperties applicationProperties;
    private IndexAdminManager adminManager;
    private DefaultSchedulerProperties defaultSchedulerProperties;

    @Before
    public void setUp() throws Exception {
        indexLifecycleManager = context.mock(IndexLifecycleManager.class);
        globalPermissionManager = context.mock(GlobalPermissionManager.class);
        taskManager = context.mock(TaskManager.class);
        authenticationContext = context.mock(JiraAuthenticationContext.class);
        outlookDateManager = context.mock(OutlookDateManager.class);
        indexPathManager = context.mock(IndexPathManager.class);
        applicationProperties = context.mock(ApplicationProperties.class);
        this.i18n = context.mock(I18nHelper.class);

        defaultSchedulerProperties = new DefaultSchedulerProperties();
        final QuartzSchedulerResources resources = new QuartzSchedulerResources();
        resources.setName("Test");
        resources.setJobStore(new RAMJobStore());
        this.scheduler = new StdScheduler(new QuartzScheduler(resources, new SchedulingContext(), 1000, 1000), new SchedulingContext());

        context.checking(new Expectations() {{
            allowing(authenticationContext).getI18nHelper();
            will(returnValue(i18n));
        }});

        context.checking(new Expectations() {{
            allowing(i18n).getText(IndexKeys.GROUP_NAME);
            will(returnValue("iDalko"));
            allowing(i18n).getText(IndexKeys.JOB_NAME);
            will(returnValue("iDalkoScheduledReIndexerJob"));
            allowing(i18n).getText(IndexKeys.BANNER_JOB_NAME);
            will(returnValue("iDalkoAnnouncementJob"));
            allowing(i18n).getText(IndexKeys.STOP_JOB_NAME);
            will(returnValue("iDalkoStopReIndexerJob"));
            allowing(i18n).getText(IndexKeys.TRIGGER_NAME);
            will(returnValue("iDalkoAutomaticIndexer"));
            allowing(i18n).getText(IndexKeys.BANNER_TRIGGER_NAME);
            will(returnValue("iDalkoAnnouncementTrigger"));
            allowing(i18n).getText(IndexKeys.REINDEX_CHECKER_TRIGGER);
            will(returnValue("iDalkoReIndexerChecker"));
            allowing(i18n).getText(IndexKeys.STOP_TRIGGER_NAME);
            will(returnValue("iDalkoStopReIndexerTrigger"));
            allowing(i18n).getText(IndexKeys.ANNOUNCEMENT_TEXT);
            will(returnValue("announcementBanner.text"));
            allowing(i18n).getText(IndexKeys.ANNOUNCEMENT_VISIBILITY);
            will(returnValue("announcementBanner.visibility"));
        }});

        context.checking(new Expectations() {{
            allowing(authenticationContext).getI18nHelper();
            will(returnValue(i18n));
        }});

        adminManager = new IndexAdminManagerImpl(indexLifecycleManager, globalPermissionManager, taskManager, authenticationContext, outlookDateManager, indexPathManager, applicationProperties);
        assertNotNull(adminManager);


        schedulerService = new SchedulerServiceImpl() {
            @Override
            protected Scheduler getScheduler() {
                return IndexServiceTest.this.scheduler;
            }

            @Override
            protected I18nHelper getI18Helper() {
                return i18n;
            }

            @Override
            protected ScheduleProperties createProperties() {
                return defaultSchedulerProperties;
            }
        };


        indexService = new IndexServiceImpl(adminManager, schedulerService) {
            @Override
            public boolean isIndexingNecessary() {
                return true;
            }
        };
        adminManager = new IndexAdminManagerImpl(indexLifecycleManager, globalPermissionManager, taskManager, authenticationContext, outlookDateManager, indexPathManager, applicationProperties);
    }

    @Test
    public void testJobScheduled() throws Exception {
        // first check if all jobs exist
        final JobDetail indexingJob = scheduler.getJobDetail(i18n.getText(IndexKeys.JOB_NAME),
                i18n.getText(IndexKeys.GROUP_NAME));

        assertNotNull(indexingJob);

        final JobDetail bannerJobName = scheduler.getJobDetail(i18n.getText(IndexKeys.BANNER_JOB_NAME),
                i18n.getText(IndexKeys.GROUP_NAME));
        assertNotNull(bannerJobName);


        final JobDetail stopJobName = scheduler.getJobDetail(i18n.getText(IndexKeys.STOP_JOB_NAME),
                i18n.getText(IndexKeys.GROUP_NAME));
        assertNotNull(stopJobName);

        final JobDetail notExistedJob = scheduler.getJobDetail("notExistedJob",
                i18n.getText(IndexKeys.GROUP_NAME));
        assertNull(notExistedJob);
    }

    @Test
    public void testAnnouncementJobExecution() throws Exception {
        context.checking(new Expectations() {{
            one(applicationProperties).getDefaultBackedString(APKeys.JIRA_ALERT_HEADER);
            will(returnValue("JIRA_ALERT_HEADER"));
            one(applicationProperties).getDefaultBackedString(APKeys.JIRA_ALERT_HEADER_VISIBILITY);
            will(returnValue("public"));
            one(applicationProperties).setText(APKeys.JIRA_ALERT_HEADER, "announcementBanner.text");
            one(applicationProperties).setText(APKeys.JIRA_ALERT_HEADER_VISIBILITY, "announcementBanner.visibility");
        }});

        final AnnounceIndexingJob announceIndexingJob = new AnnounceIndexingJob() {
            @Override
            protected IndexService findService() {
                return indexService;
            }
        };

        // jmock will check setting of applicationProperties internally
        announceIndexingJob.execute(null);
    }

    @Test
    public void testStopJobExecution() throws Exception {
        // checking for banner removing
        context.checking(new Expectations() {{
            one(applicationProperties).setText(APKeys.JIRA_ALERT_HEADER, null);
            one(applicationProperties).setText(APKeys.JIRA_ALERT_HEADER_VISIBILITY, null);
        }});

        final StopReIndexerJob stopReIndexerJob = new StopReIndexerJob() {
            @Override
            protected IndexService findService() {
                return indexService;
            }
        };

        // jmock will check setting of applicationProperties internally
        stopReIndexerJob.execute(null);

        // should be no re-scheduler trigger
        final Trigger trigger = getTrigger(IndexKeys.REINDEX_CHECKER_TRIGGER);
        assertNull(trigger);
    }

    @Test
    public void testIndexingJobExecution() throws Exception {
        // checks for banner removing
        context.checking(new Expectations() {{
            one(applicationProperties).setText(APKeys.JIRA_ALERT_HEADER, null);
            one(applicationProperties).setText(APKeys.JIRA_ALERT_HEADER_VISIBILITY, null);
        }});

        final boolean[] isIndexed = new boolean[]{false};
        // test indexing if no any active user available
        indexService.setIndexAdminManager(new IndexAdminManagerImpl(indexLifecycleManager,
                globalPermissionManager,
                taskManager,
                authenticationContext,
                outlookDateManager,
                indexPathManager,
                applicationProperties) {

            public String startIndexing() throws Exception {
                isIndexed[0] = true;
                return "";
            }

            public boolean isUsersInactive(ScheduleProperties properties) {
                return true;
            }
        });
        final IndexingJob job = new IndexingJob() {
            @Override
            protected IndexService findService() {
                return indexService;
            }
        };

        job.execute(null);
        // indexing should be made
        assertTrue("Indexing should be done", isIndexed[0]);
    }

    @Test
    public void testIndexingJobExecutionIfNecessary() throws Exception {
        final IndexServiceImpl indexServiceIfNecessary = new IndexServiceImpl(adminManager, schedulerService) {
            @Override
            public boolean isIndexingNecessary() {
                return false;
            }
        };

        // checks for banner removing
        context.checking(new Expectations() {{
            one(applicationProperties).setText(APKeys.JIRA_ALERT_HEADER, null);
            one(applicationProperties).setText(APKeys.JIRA_ALERT_HEADER_VISIBILITY, null);
        }});

        defaultSchedulerProperties.setIndexingNecessary(true);

        final boolean[] isIndexed = new boolean[]{false};
        // test indexing if no any active user available
        indexServiceIfNecessary.setIndexAdminManager(new IndexAdminManagerImpl(indexLifecycleManager,
                globalPermissionManager,
                taskManager,
                authenticationContext,
                outlookDateManager,
                indexPathManager,
                applicationProperties) {

            public String startIndexing() throws Exception {
                isIndexed[0] = true;
                return "";
            }

            public boolean isUsersInactive(ScheduleProperties properties) {
                return true;
            }
        });
        final IndexingJob job = new IndexingJob() {
            @Override
            protected IndexService findService() {
                return indexServiceIfNecessary;
            }
        };

        job.execute(null);
        // indexing should be made
        assertFalse("Indexing should be done", isIndexed[0]);
    }

    @Test
    public void testIndexingJobReschedule() throws Exception {
        final boolean[] isIndexed = new boolean[]{false};
        // test indexing if no any active user available
        indexService.setIndexAdminManager(new IndexAdminManagerImpl(indexLifecycleManager,
                globalPermissionManager,
                taskManager,
                authenticationContext,
                outlookDateManager,
                indexPathManager,
                applicationProperties) {

            public String startIndexing() throws Exception {
                isIndexed[0] = true;
                return "";
            }

            public boolean isUsersInactive(ScheduleProperties properties) {
                return false;
            }
        });
        final IndexingJob job = new IndexingJob() {
            @Override
            protected IndexService findService() {
                return indexService;
            }
        };

        job.execute(null);
        assertFalse("Indexing should not start!", isIndexed[0]);

        final Trigger trigger = getTrigger(IndexKeys.REINDEX_CHECKER_TRIGGER);

        // let's assume that we don't need to recalculate time
        assertTrue(schedulerService.getScheduleProperties().getIndexingMinute() + schedulerService.getScheduleProperties().getReindexingIntervalInMinutes() < 60);
        final Calendar instance = Calendar.getInstance();
        instance.add(Calendar.MINUTE, schedulerService.getScheduleProperties().getReindexingIntervalInMinutes());
        assertNextFireTime(trigger, instance.get(Calendar.HOUR_OF_DAY), instance.get(Calendar.MINUTE));
    }


    @Test
    public void testNextFireTime() throws Exception {
        final Trigger bannerTrigger = getTrigger(IndexKeys.BANNER_TRIGGER_NAME);
        assertNextFireTime(bannerTrigger, schedulerService.getScheduleProperties().getAnnouncedHour(), schedulerService.getScheduleProperties().getAnnounceMinutes());

        final Trigger indexerTrigger = getTrigger(IndexKeys.TRIGGER_NAME);
        assertNextFireTime(indexerTrigger, schedulerService.getScheduleProperties().getIndexingHour(), schedulerService.getScheduleProperties().getIndexingMinute());

        final Trigger stopTrigger = getTrigger(IndexKeys.STOP_TRIGGER_NAME);
        assertNextFireTime(stopTrigger, schedulerService.getScheduleProperties().getStopReindexerHour(), schedulerService.getScheduleProperties().getStopReindexerMinute());
    }

    private Trigger getTrigger(String triggerName) throws SchedulerException {
        return scheduler.getTrigger(i18n.getText(triggerName), i18n.getText(IndexKeys.GROUP_NAME));
    }

    private void assertNextFireTime(Trigger trigger, int hour, int minutes) {
        assertNotNull(trigger);
        final Date nextFireTime = trigger.getNextFireTime();
        assertDate(hour, minutes, nextFireTime);
    }

    private void assertDate(int hour, int minutes, Date nextFireTime) {
        final Calendar instance = Calendar.getInstance();
        instance.setTime(nextFireTime);
        assertEquals(hour, instance.get(Calendar.HOUR_OF_DAY));
        assertEquals(minutes, instance.get(Calendar.MINUTE));
    }
}