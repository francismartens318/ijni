/**
 * 
 * Copyright (C) 2011, iDalko BVBA
 * All rights reserved.
 * 
 * THIS SOURCE CODE IS AN UNPUBLISHED PROPRIETARY TRADE SECRET OF iDalko BVBA, unless stated otherwise
 * in the license text.
 * 
 * The copyright notice above does not evidence any actual or intended publication of such source code.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, 
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF 
 * THE POSSIBILITY OF SUCH DAMAGE
 */
package com.idalko.jira.plugins;

import com.idalko.jira.plugins.indexer.ScheduleProperties;

/**
 * andrew Dec 2, 2010 11:53:56 AM
 */
public class DefaultSchedulerProperties implements ScheduleProperties {
    private String announcement;
    private String bannerVisibility;
    private boolean isOwnBanner;
    private boolean isEnabled = true;
    private boolean isIndexingNecessary;

    public int getReindexingIntervalInMinutes() {
        return REINDEXING_INTERVAL_IN_MINUTES;
    }

    public int getAnnouncedHour() {
        return ANNOUNCED_HOUR;
    }

    public int getAnnounceMinutes() {
        return ANNOUNCE_MINUTES;
    }

    public int getIndexingHour() {
        return INDEXING_HOUR;
    }

    public int getIndexingMinute() {
        return INDEXING_MINUTE;
    }

    public int getStopReindexerHour() {
        return STOP_REINDEXER_HOUR;
    }

    public int getStopReindexerMinute() {
        return STOP_REINDEXER_MINUTE;
    }

    public String getAnnouncementText() {
        return "announcementBanner.text";
    }

    public Boolean getIsCheckUsersOnline() {
        return true;
    }

    public void setDefaultBanner(String announcement) {
        this.announcement = announcement;
    }

    public void setDefaultVisibility(String bannerVisibility) {
        this.bannerVisibility = bannerVisibility;
    }

    public boolean isOwnBannerShown() {
        return isOwnBanner;
    }

    public String getDefaultBanner() {
        return announcement;
    }

    public String getDefaultVisibility() {
        return bannerVisibility;
    }

    public void setOwnBannerShown(Boolean value) {
        this.isOwnBanner = value;
    }

    public void saveProperty(String name, String value) {

    }

    public Integer getPropertyInt(String param) {
        return null;
    }

    public String getProperty(String announcementText) {
        return null;
    }

    public Boolean getBooleanProperty(String checkUsersOnline) {
        return null;
    }

    public void copyTo(ScheduleProperties target) {

    }

    public Boolean getIsEnable() {
        return isEnabled;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnabled = isEnabled;
    }

    public Boolean getIsIndexingNecessary() {
        return isIndexingNecessary;
    }


    public void setIndexingNecessary(boolean indexingNecessary) {
        isIndexingNecessary = indexingNecessary;
    }
}
